# PiBot
Library for controlling the PiBot

# Installation
```bash
pip install pibot
```

# Usage
```python
from pibot import *
display.sample_function("test")
```